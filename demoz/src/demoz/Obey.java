package demoz;

public class Obey {
	//whatsoever
	private String name = "MASTER";
	private int savage = 123;
	
	public int getSavage() {
		return savage;
	}
	public void setSavage(int savage) {
		this.savage = savage;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void mustSay(){
		System.out.println("YOU MUST SAY HELLO TO ME! -World");
	}
	public void doSomething(){
		System.out.println("I'm not doing anything now!");
	}

}
